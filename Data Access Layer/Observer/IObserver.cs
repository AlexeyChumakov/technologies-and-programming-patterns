﻿namespace Data_Access_Layer.Observer
{
    public interface IObserver
    {
        void Update(string type, ISubject subject);
    }
}
