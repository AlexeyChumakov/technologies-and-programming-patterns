﻿using Data_Access_Layer.Domain;
using Data_Access_Layer.Observer;
using Data_Access_Layer.Observer.Listeners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Data_Access_Layer.Data_Access_Object
{
	public class RouteDAO : IDAO<Route>
	{
		private readonly string connectionString;
		private EventManager events = new EventManager();

		public RouteDAO(string connectionString)
		{
			this.connectionString = connectionString;
			events.Attach(new RouteDomainListener());
		}

		public IEnumerable<Route> GetAll()
		{
			List<Route> routes = new List<Route>();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetRoute", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var route = new Route()
					{
						id_route = Convert.ToInt32(rdr["id_route"]),
						point_of_departure = rdr["point_of_departure"].ToString(),
						point_of_arrival = rdr["point_of_arrival"].ToString(),
						on_way = Convert.ToDateTime(rdr["on_way"]),
						distance_km = (float)Convert.ToDouble(rdr["distance_km"])
					};
					routes.Add(route);
				}
				events.Notify("Select entities from");
				return routes;
			}
		}

		public Route GetById(int id)
		{
			Route route = new Route();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetRoute", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					route.id_route = Convert.ToInt32(rdr["id_route"]);
					route.point_of_departure = rdr["point_of_departure"].ToString();
					route.point_of_arrival = rdr["point_of_arrival"].ToString();
					route.on_way = Convert.ToDateTime(rdr["on_way"]);
					route.distance_km = (float)Convert.ToDouble(rdr["distance_km"]);
				}
				events.Notify("Select entity from");
				return route;
			}
		}

		public void Insert(Route obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spAddNew", con);
				con.Open();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@point_of_departure", obj.point_of_departure);
				cmd.Parameters.AddWithValue("@point_of_arrival", obj.point_of_arrival);
				cmd.Parameters.AddWithValue("@on_way", obj.on_way);
				cmd.Parameters.AddWithValue("@distance_km", obj.distance_km);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Update(Route obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spUpdateRoute", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", obj.id_route);
				cmd.Parameters.AddWithValue("@point_of_departure", obj.point_of_departure);
				cmd.Parameters.AddWithValue("@point_of_arrival", obj.point_of_arrival);
				cmd.Parameters.AddWithValue("@on_way", obj.on_way);
				cmd.Parameters.AddWithValue("@distance_km", obj.distance_km);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Update entity from");
		}

		public void Delete(Route obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spDeleteRoute", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", obj.id_route);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Delete entity from");
		}
	}
}
