﻿using Data_Access_Layer.Data_Access_Object;
using Data_Access_Layer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data_Access_Layer.Proxy
{
	public class RealSubject
	{
        private readonly PlaceDAO placeDAO;
        private readonly RouteDAO routeDAO;
        private readonly TrainDAO trainDAO;
        private readonly WagonDAO wagonDAO;
        private readonly UserDAO userDAO;

        public RealSubject(string stringconnection)
        {
            placeDAO = new PlaceDAO(stringconnection);
            routeDAO = new RouteDAO(stringconnection);
            trainDAO = new TrainDAO(stringconnection);
            wagonDAO = new WagonDAO(stringconnection);
            userDAO = new UserDAO(stringconnection);
        }

        public void AllPlaces()
        {
            IEnumerable<Place> places = placeDAO.GetAll();

            foreach (Place place in places)
            {
                Console.WriteLine($"[id_place = {place.id_place}; place_number = {place.place_number}; is_free = {place.is_free}; fk_wagon = {place.fk_wagon};]");
            }
        }

        public void AllRoutes()
        {
            IEnumerable<Route> routes = routeDAO.GetAll();

            foreach (Route route in routes)
            {
                Console.WriteLine($"[id_route = {route.id_route}; point_of_departure = {route.point_of_departure}; point_of_arrival = {route.point_of_arrival}; on_way = {route.on_way}; distance_km = {route.distance_km}]");
            }
        }

        public void AllTrains()
        {
            IEnumerable<Train> trains = trainDAO.GetAll();

            foreach (Train train in trains)
            {
                Console.WriteLine($"[id_train = {train.id_train}; train_key = {train.train_key}; fk_route = {train.fk_route}; departing = {train.departing};  arriving = {train.arriving};]");
            }
        }

        public void AllWagons()
        {
            IEnumerable<Wagon> wagons = wagonDAO.GetAll();

            foreach (Wagon wagon in wagons)
            {
                Console.WriteLine($"[id_wagon = {wagon.id_wagon}; wagon_number = {wagon.wagon_number}; fk_train = {wagon.fk_train}; places = {wagon.places}; type_wagon = {wagon.type_wagon}; ticket_price = {wagon.ticket_price};]");
            }
        }

        public void GetPlace()
        {
            Console.WriteLine("Enter place id: ");
            int id = int.Parse(Console.ReadLine());

            Place place = placeDAO.GetById(id);

            Console.WriteLine($"[id_place = {place.id_place}; place_number = {place.place_number}; is_free = {place.is_free}; fk_wagon = {place.fk_wagon};]");
        }

        public void GetRoute()
        {
            Console.WriteLine("Enter route id: ");
            int id = int.Parse(Console.ReadLine());

            Route route = routeDAO.GetById(id);

            Console.WriteLine($"[id_route = {route.id_route}; point_of_departure = {route.point_of_departure}; point_of_arrival = {route.point_of_arrival}; on_way = {route.on_way}; distance_km = {route.distance_km}]");
        }

        public void GetTrain()
        {
            Console.WriteLine("Enter train id: ");
            int id = int.Parse(Console.ReadLine());

            Train train = trainDAO.GetById(id);

            Console.WriteLine($"[id_train = {train.id_train}; train_key = {train.train_key}; fk_route = {train.fk_route}; departing = {train.departing};  arriving = {train.arriving};]");
        }

        public void GetWagon()
        {
            Console.WriteLine("Enter wagon id: ");
            int id = int.Parse(Console.ReadLine());

            Wagon wagon = wagonDAO.GetById(id);

            Console.WriteLine($"[id_wagon = {wagon.id_wagon}; wagon_number = {wagon.wagon_number}; fk_train = {wagon.fk_train}; places = {wagon.places}; type_wagon = {wagon.type_wagon}; ticket_price = {wagon.ticket_price};]");
            
        }

        public void AddPlace()
        {
            Console.WriteLine("Enter place id: ");
            int id_place = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter place number: ");
            int place_number = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter is place free: ");
            bool is_free = bool.Parse(Console.ReadLine());

            Console.WriteLine("Enter fk wagon: ");
            int fk_wagon = int.Parse(Console.ReadLine());

            if (placeDAO.GetById(id_place) is not null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such place exists already");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            placeDAO.Insert(new Place { id_place = id_place, fk_wagon = fk_wagon, is_free = is_free, place_number = place_number });
        }

        public void DeletePlace()
        {
            Console.WriteLine("Enter place id: ");
            int id_place = int.Parse(Console.ReadLine());

            Place place = placeDAO.GetById(id_place);

            if (place is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such place not exists");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            placeDAO.Delete(place);
        }

        public void AddRoute()
        {
            Console.WriteLine("Enter route id: ");
            int id_route = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter point of departure: ");
            string point_of_departure = Console.ReadLine();

            Console.WriteLine("Enter point of arrival: ");
            string point_of_arrival = Console.ReadLine();

            Console.WriteLine("Enter distance km: ");
            int distance_km = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter on way: ");
            DateTime on_way = DateTime.Parse(Console.ReadLine());

            if (routeDAO.GetById(id_route) is not null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such route exists already");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            routeDAO.Insert(new Route { id_route = id_route, point_of_departure = point_of_departure, point_of_arrival = point_of_arrival, distance_km = distance_km, on_way = on_way});
        }

        public void DeleteRoute()
        {
            Console.WriteLine("Enter route id: ");
            int id_route = int.Parse(Console.ReadLine());

            Route route = routeDAO.GetById(id_route);

            if (route is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such route doesn't exist");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            routeDAO.Delete(route);
        }

        public void AddTrain()
        {
            Console.WriteLine("Enter train id: ");
            int id_train = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter departing: ");
            DateTime departing = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter arriving: ");
            DateTime arriving = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter fk route: ");
            int fk_route = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter train key: ");
            char train_key = char.Parse(Console.ReadLine());

            if (trainDAO.GetById(id_train) is not null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such train exists already");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            trainDAO.Insert(new Train { id_train = id_train, departing = departing, arriving = arriving, fk_route = fk_route, train_key = train_key});
        }

        public void DeleteTrain()
        {
            Console.WriteLine("Enter train id: ");
            int id_train = int.Parse(Console.ReadLine());

            Train train = trainDAO.GetById(id_train);

            if (train is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such train doesn't exist");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            trainDAO.Delete(train);
        }

        public void AddWagon()
        {
            Console.WriteLine("Enter wagon id: ");
            int id_wagon = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter fk_train: ");
            int fk_train = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter ticket price: ");
            float ticket_price = float.Parse(Console.ReadLine());

            Console.WriteLine("Enter wagon number: ");
            int wagon_number = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter type wagon: ");
            string type_wagon = Console.ReadLine();

            if (wagonDAO.GetById(id_wagon) is not null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such wagon exists already");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            wagonDAO.Insert(new Wagon { id_wagon = id_wagon, fk_train = fk_train, ticket_price = ticket_price, wagon_number = wagon_number, type_wagon = type_wagon});
        }

        public void DeleteWagon()
        {
            Console.WriteLine("Enter wagon id: ");
            int id_wagon = int.Parse(Console.ReadLine());

            Wagon wagon = wagonDAO.GetById(id_wagon);

            if (wagon is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Such wagon doesn't exist");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            wagonDAO.Delete(wagon);
        }

        public User LogIn()
        {
            User user = null;

            while (user is null)
            {
                Console.WriteLine("Enter login:");
                string login = Console.ReadLine();

                Console.WriteLine("Enter password:");
                string password = Console.ReadLine();

                user = userDAO.GetAll()
                    .SingleOrDefault(x => x.login == login && x.password == password);

                if (user is null)
                {
                    Console.WriteLine("Wrong login or password");
                }
            }

            return user;
        }

        public User Register()
        {
            User user = null;

            while (user is null)
            {
                Console.WriteLine("Enter login:");
                string login = Console.ReadLine();

                Console.WriteLine("Enter password:");
                string password = Console.ReadLine();

                Console.WriteLine("Repeat password:");
                string password2 = Console.ReadLine();

                if(password != password2)
				{
                    Console.WriteLine("Password and repeated password are different");
                    continue;
				}

                if (userDAO.GetAll().SingleOrDefault(x => x.login == login) is not null)
                {
                    Console.WriteLine("This login is taken");
                    continue;
                }

                user = new User { login = login, password = password, id_role = 1, id_user = userDAO.GetAll().Count() + 1 };

                userDAO.Insert(user);
            }

            return user;
        }


        public void Run()
        {
            ConsoleKeyInfo key1 = Console.ReadKey();
            ConsoleKeyInfo key2 = Console.ReadKey();
            Dictionary<ConsoleKey, Action> action = new Dictionary<ConsoleKey, Action>();

            if (key1.Key == ConsoleKey.S)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(AllPlaces));
                action.Add(ConsoleKey.R, new Action(AllRoutes));
                action.Add(ConsoleKey.T, new Action(AllTrains));
                action.Add(ConsoleKey.W, new Action(AllWagons));
            }
            if (key1.Key == ConsoleKey.G)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(GetPlace));
                action.Add(ConsoleKey.R, new Action(GetRoute));
                action.Add(ConsoleKey.T, new Action(GetTrain));
                action.Add(ConsoleKey.W, new Action(GetWagon));
            }
            else if (key1.Key == ConsoleKey.A)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(AddPlace));
                action.Add(ConsoleKey.R, new Action(AddRoute));
                action.Add(ConsoleKey.T, new Action(AddTrain));
                action.Add(ConsoleKey.W, new Action(AddWagon));
            }
            else if (key1.Key == ConsoleKey.D)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(DeletePlace));
                action.Add(ConsoleKey.R, new Action(DeleteRoute));
                action.Add(ConsoleKey.T, new Action(DeleteTrain));
                action.Add(ConsoleKey.W, new Action(DeleteWagon));
            }

            action[key2.Key].Invoke();

            Run();
        }
    }
}


