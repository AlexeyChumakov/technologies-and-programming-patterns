﻿using Data_Access_Layer.Data_Access_Object;
using Data_Access_Layer.Singleton;

namespace Data_Access_Layer.Factory
{
	public class TrainDAOFactory : DAOFactory<TrainDAO>
	{
		public override TrainDAO FactoryMethod()
		{
			return TrainDAOSingleton.GetInstance(connectionString);
		}
	}
}
