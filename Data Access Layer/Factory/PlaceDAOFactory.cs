﻿using Data_Access_Layer.Data_Access_Object;
using Data_Access_Layer.Singleton;

namespace Data_Access_Layer.Factory
{
	public class PlaceDAOFactory : DAOFactory<PlaceDAO>
	{
		public override PlaceDAO FactoryMethod()
		{
			return PlaceDAOSingleton.GetInstance(connectionString);
		}
	}
}
