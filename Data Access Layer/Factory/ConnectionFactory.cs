﻿namespace Data_Access_Layer.Factory
{
    class ConnectionFactory
    {
        private readonly string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=RailwayDB;Integrated Security=True";

        public string GetConnectionString() => connectionString;
    }
}
