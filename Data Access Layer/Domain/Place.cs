﻿namespace Data_Access_Layer.Domain
{
	public class Place
	{
		public int id_place { get; set; }
		public int place_number { get; set; }
		public bool is_free { get; set; }
		public int fk_wagon { get; set; }
	}
}

