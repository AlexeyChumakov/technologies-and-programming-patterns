﻿using Data_Access_Layer.Data_Access_Object;

namespace Data_Access_Layer.Singleton
{
    public class PlaceDAOSingleton : DAOSingleton<PlaceDAO>
    {
        public static new PlaceDAO GetInstance(string connectionString)
        {
            if (DAO == null)
            {
                DAO = new PlaceDAO(connectionString);
            }

            return DAO;
        }
    }
}
