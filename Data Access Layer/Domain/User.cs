﻿namespace Data_Access_Layer.Domain
{
	public class User
	{
		public int id_user { get; set; }
		public int id_role { get; set; }

		public string login { get; set; }
		public string password { get; set; }
	}
}
