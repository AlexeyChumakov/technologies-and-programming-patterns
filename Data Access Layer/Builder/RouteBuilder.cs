﻿using Data_Access_Layer.Domain;
using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Builder
{
	public class RouteBuilder
	{
		private readonly Route route;

		public RouteBuilder()
		{
			route = new Route();
			route.trains = new List<Train>();
		}

		public Route Build()
		{
			return new Route() {
				id_route = route.id_route,
				point_of_departure = route.point_of_departure,
				point_of_arrival = route.point_of_arrival,
				on_way = route.on_way,
				distance_km = route.distance_km,
				trains = new List<Train>(route.trains)
			}; 
		}

		public RouteBuilder AddTrain(Train train)
		{
			route.trains.Add(train);
			return this;
		}

		public RouteBuilder SetId_route(int id_route)
		{
			route.id_route = id_route;
			return this;
		}

		public RouteBuilder SetPoint_of_departure(string point_of_departure)
		{
			route.point_of_departure = point_of_departure;
			return this;
		}

		public RouteBuilder SetPoint_of_arrival(string point_of_arrival)
		{
			route.point_of_arrival = point_of_arrival;
			return this;
		}

		public RouteBuilder SetOn_way(DateTime on_way)
		{
			route.on_way = on_way;
			return this;
		}

		public RouteBuilder SetDistance_km(float distance_km)
		{
			route.distance_km = distance_km;
			return this;
		}
	}
}
