﻿using System;

namespace Data_Access_Layer.Observer.Listeners
{
	class UserDomainListener : IObserver
    {
        public void Update(string type, ISubject subject)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(type + " user table");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
