﻿using System;

namespace Data_Access_Layer.Observer.Listeners
{
	class PlaceDomainListener : IObserver
    {
        public void Update(string type, ISubject subject)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(type + " place table");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
