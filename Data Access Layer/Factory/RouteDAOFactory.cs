﻿using Data_Access_Layer.Data_Access_Object;
using Data_Access_Layer.Singleton;

namespace Data_Access_Layer.Factory
{
	public class RouteDAOFactory : DAOFactory<RouteDAO>
	{
		public override RouteDAO FactoryMethod()
		{
			return RouteDAOSingleton.GetInstance(connectionString);
		}
	}
}
