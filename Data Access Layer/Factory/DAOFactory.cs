﻿using Data_Access_Layer.Singleton;

namespace Data_Access_Layer.Factory
{
	public abstract class DAOFactory<T>
	{
		protected readonly string connectionString = @"Server=DESKTOP-SV7KP7O;Initial Catalog=RailwayDB;Integrated Security=True";

		public abstract T FactoryMethod();
	}
}
