﻿namespace Data_Access_Layer.Singleton
{
    public abstract class DAOSingleton<T>
    {
        protected static T DAO;

        public static T GetInstance(string connectionString)
        {
            if (DAO == null)
            {
                DAO = default(T);
            }

            return DAO;
        }
    }
}
