﻿using System;

namespace Data_Access_Layer.Observer.Listeners
{
	class RouteDomainListener : IObserver
    {
        public void Update(string type, ISubject subject)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(type + " route table");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
