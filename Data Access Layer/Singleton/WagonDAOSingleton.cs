﻿using Data_Access_Layer.Data_Access_Object;

namespace Data_Access_Layer.Singleton
{
    public class WagonDAOSingleton : DAOSingleton<WagonDAO>
    {
        public static new WagonDAO GetInstance(string connectionString)
        {
            if (DAO == null)
            {
                DAO = new WagonDAO(connectionString);
            }

            return DAO;
        }
    }
}
