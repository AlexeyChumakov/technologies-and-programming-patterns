﻿using Data_Access_Layer.Data_Access_Object;

namespace Data_Access_Layer.Singleton
{
	public class RouteDAOSingleton : DAOSingleton<RouteDAO>
	{
        public static new RouteDAO GetInstance(string connectionString)
        {
            if (DAO == null)
            {
                DAO = new RouteDAO(connectionString);
            }

            return DAO;
        }
    }
}
