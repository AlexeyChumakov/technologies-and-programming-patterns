﻿using System;

namespace Data_Access_Layer.Observer.Listeners
{
	class WagonDomainListener : IObserver
    {
        public void Update(string type, ISubject subject)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(type + " wagon table");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
