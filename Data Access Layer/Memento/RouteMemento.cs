﻿using Data_Access_Layer.Domain;
using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Memento
{
    public class RouteMemento
    {
        public int id_route { get; set; }
        public string point_of_departure { get; set; }
        public string point_of_arrival { get; set; }
        public DateTime on_way { get; set; }
        public float distance_km { get; set; }

        public List<Train> trains { get; set; }
    }

}
