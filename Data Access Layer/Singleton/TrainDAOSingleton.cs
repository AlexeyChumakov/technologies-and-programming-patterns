﻿using Data_Access_Layer.Data_Access_Object;

namespace Data_Access_Layer.Singleton
{
	public class TrainDAOSingleton : DAOSingleton<TrainDAO>
	{
        public static new TrainDAO GetInstance(string connectionString)
        {
            if (DAO == null)
            {
                DAO = new TrainDAO(connectionString);
            }

            return DAO;
        }
    }
}
