﻿using Data_Access_Layer.Domain;
using System;

namespace Data_Access_Layer.Builder
{
	public class TrainBuilder
	{
		private readonly Train train;

		public TrainBuilder() => train = new Train();


		public Train Build()
		{
			return train;
		}

		public TrainBuilder AddWagon(Wagon wagon)
		{
			train.wagons.Add(wagon);
			return this;
		}

		public TrainBuilder SetId_train(int id_train)
		{
			train.id_train = id_train;
			return this;
		}

		public TrainBuilder SetTrain_key(char train_key)
		{
			train.train_key = train_key;
			return this;
		}

		public TrainBuilder SetFk_route(int fk_route)
		{
			train.fk_route = fk_route;
			return this;
		}

		public TrainBuilder SetDeparting(DateTime departing)
		{
			train.departing = departing;
			return this;
		}

		public TrainBuilder SetArriving(DateTime arriving)
		{
			train.arriving = arriving;
			return this;
		}
	}
}
