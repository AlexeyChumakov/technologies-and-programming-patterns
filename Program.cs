﻿using Data_Access_Layer.Proxy;
using System;

namespace Technologies_and_programming_patterns
{
	class Program
	{
		static void Main()
		{
			RealSubject view = new RealSubject(@"Server=DESKTOP-SV7KP7O;Initial Catalog=RailwayDB;Integrated Security=True");
			Proxy proxy = new Proxy(view);
			proxy.Run();
		}
	}
}
