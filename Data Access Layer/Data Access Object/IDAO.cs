﻿using System.Collections.Generic;

namespace Data_Access_Layer.Data_Access_Object
{
	public interface IDAO<T>
	{
		IEnumerable<T> GetAll();
		T GetById(int id);
		void Insert(T obj);
		void Update(T obj);
		void Delete(T obj);
	}
}
