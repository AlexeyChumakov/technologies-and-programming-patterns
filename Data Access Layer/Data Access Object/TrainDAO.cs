﻿using Data_Access_Layer.Domain;
using Data_Access_Layer.Observer;
using Data_Access_Layer.Observer.Listeners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Data_Access_Layer.Data_Access_Object
{
	public class TrainDAO : IDAO<Train>
	{
		private readonly string connectionString;
		private EventManager events = new EventManager();

		public TrainDAO(string connectionString)
		{
			this.connectionString = connectionString;
			events.Attach(new TrainDomainListener());
		}

		public IEnumerable<Train> GetAll()
		{
			List<Train> trains = new List<Train>();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetTrain", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var train = new Train()
					{
						id_train = Convert.ToInt32(rdr["id_train"]),
						train_key = char.Parse(rdr["train_key"].ToString()),
						fk_route = Convert.ToInt32(rdr["fk_route"]),
						departing = Convert.ToDateTime(rdr["departing"]),
						arriving = Convert.ToDateTime(rdr["arriving"])
					};
					trains.Add(train);
				}
				events.Notify("Select entities from");
				return trains;
			}
		}

		public Train GetById(int id)
		{
			Train train = new Train();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetTrain", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					train.id_train = Convert.ToInt32(rdr["id_train"]);
					train.train_key = char.Parse(rdr["train_key"].ToString());
					train.fk_route = Convert.ToInt32(rdr["fk_route"]);
					train.departing = Convert.ToDateTime(rdr["departing"]);
					train.arriving = Convert.ToDateTime(rdr["arriving"]);
				}
				events.Notify("Select entity from");
				return train;
			}
		}

		public void Insert(Train obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spAddNew", con);
				con.Open();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@train_key", obj.train_key);
				cmd.Parameters.AddWithValue("@fk_route", obj.fk_route);
				cmd.Parameters.AddWithValue("@departing", obj.departing);
				cmd.Parameters.AddWithValue("@arriving", obj.arriving);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Update(Train obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spUpdateWagon", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_train", obj.id_train);
				cmd.Parameters.AddWithValue("@train_key", obj.train_key);
				cmd.Parameters.AddWithValue("@fk_route", obj.fk_route);
				cmd.Parameters.AddWithValue("@departing", obj.departing);
				cmd.Parameters.AddWithValue("@arriving", obj.arriving);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Update entity from");
		}

		public void Delete(Train obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spDeleteTrain", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_train", obj.id_train);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Delete entity from");
		}
	}
}

