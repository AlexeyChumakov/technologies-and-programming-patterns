﻿using Data_Access_Layer.Domain;

namespace Data_Access_Layer.Builder
{
	public class PlaceBuilder
	{
		private readonly Place place;

		public PlaceBuilder() => place = new Place();


		public Place Build()
		{
			return place;
		}

		public PlaceBuilder SetId_place(int id_place)
		{
			place.id_place = id_place;
			return this;
		}

		public PlaceBuilder SetPlace_number(int place_number)
		{
			place.place_number = place_number;
			return this;
		}

		public PlaceBuilder SetIs_free(bool is_free)
		{
			place.is_free = is_free;
			return this;
		}

		public PlaceBuilder SetFk_wagon(int fk_wagon)
		{
			place.fk_wagon = fk_wagon;
			return this;
		}
	}
}

