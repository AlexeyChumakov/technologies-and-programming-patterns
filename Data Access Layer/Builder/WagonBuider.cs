﻿using Data_Access_Layer.Domain;

namespace Data_Access_Layer.Builder
{
	public class WagonBuider
	{
		private readonly Wagon wagon;

		public WagonBuider() => wagon = new Wagon();


		public Wagon Build()
		{
			return wagon;
		}

		public WagonBuider AddPlace(Place place)
		{
			wagon.places.Add(place);
			return this;
		}

		public WagonBuider SetId_wagon(int id_wagon)
		{
			wagon.id_wagon = id_wagon;
			return this;
		}

		public WagonBuider SetFk_train(int fk_train)
		{
			wagon.fk_train = fk_train;
			return this;
		}

		public WagonBuider SetType_wagon(string type_wagon)
		{
			wagon.type_wagon = type_wagon;
			return this;
		}

		public WagonBuider SetWagon_number(int wagon_number)
		{
			wagon.wagon_number = wagon_number;
			return this;
		}

		public WagonBuider SetTicket_price(float ticket_price)
		{
			wagon.ticket_price = ticket_price;
			return this;
		}
	}
}
