﻿using Data_Access_Layer.Domain;
using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Proxy
{
	public class Proxy
	{
        private RealSubject service;
        private User user;

        public Proxy(RealSubject view)
        {
            service = view;
        }

        public void AllPlaces()
        {
            service.AllPlaces();
        }

        public void AllRoutes()
        {
            service.AllRoutes();
        }

        public void AllTrains()
        {
            service.AllTrains();
        }

        public void AllWagons()
        {
            service.AllWagons();
        }

        public void GetPlace()
        {
            service.GetPlace();
        }

        public void GetRoute()
        {
            service.GetRoute();
        }

        public void GetTrain()
        {
            service.GetTrain();
        }

        public void GetWagon()
        {
            service.GetWagon();
        }

        public void AddPlace()
        {
            if (user.id_role == 2)
            {
                service.AddPlace();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void DeletePlace()
        {
            if (user.id_role == 2)
            {
                service.DeletePlace();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void AddRoute()
        {
            if (user.id_role == 2)
            {
                service.AddRoute();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void DeleteRoute()
        {
            if (user.id_role == 2)
            {
                service.DeleteRoute();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void AddTrain()
        {
            if (user.id_role == 2)
            {
                service.AddTrain();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void DeleteTrain()
        {
            if (user.id_role == 2)
            {
                service.DeleteTrain();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void AddWagon()
        {
            if (user.id_role == 2)
            {
                service.AddWagon();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void DeleteWagon()
        {
            if (user.id_role == 2)
            {
                service.DeleteWagon();
            }
            else
            {
                Console.WriteLine("No access");
            }
        }

        public void LogIn()
        {
            user = service.LogIn();
        }

        public void Register()
        {
            user = service.Register();
        }

        public void LogOut()
        {
            user = null;
        }

        public void Run()
        {
            if (user is null)
                Console.WriteLine("Please login or register");

            ConsoleKeyInfo key1 = Console.ReadKey();
            ConsoleKeyInfo key2 = Console.ReadKey();
            Dictionary<ConsoleKey, Action> action = new Dictionary<ConsoleKey, Action>();

            if (key1.Key == ConsoleKey.S)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(AllPlaces));
                action.Add(ConsoleKey.R, new Action(AllRoutes));
                action.Add(ConsoleKey.T, new Action(AllTrains));
                action.Add(ConsoleKey.W, new Action(AllWagons));
            }   
            if (key1.Key == ConsoleKey.G)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(GetPlace));
                action.Add(ConsoleKey.R, new Action(GetRoute));
                action.Add(ConsoleKey.T, new Action(GetTrain));
                action.Add(ConsoleKey.W, new Action(GetWagon));
            }
            else if (key1.Key == ConsoleKey.L)
            {
                Console.WriteLine();
                LogIn();
                Run();
            }
            else if (key1.Key == ConsoleKey.R)
            {
                Console.WriteLine();
                Register();
                Run();
            }
            else if (key1.Key == ConsoleKey.O)
            {
                Console.WriteLine();
                LogOut();
                Run();
            }
            else if (key1.Key == ConsoleKey.A)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(AddPlace));
                action.Add(ConsoleKey.R, new Action(AddRoute));
                action.Add(ConsoleKey.T, new Action(AddTrain));
                action.Add(ConsoleKey.W, new Action(AddWagon));
            }
            else if (key1.Key == ConsoleKey.D)
            {
                Console.WriteLine();
                action.Add(ConsoleKey.P, new Action(DeletePlace));
                action.Add(ConsoleKey.R, new Action(DeleteRoute));
                action.Add(ConsoleKey.T, new Action(DeleteTrain));
                action.Add(ConsoleKey.W, new Action(DeleteWagon));
            }

            action[key2.Key].Invoke();

            Run();
        }
    }
}

