﻿using Data_Access_Layer.Domain;
using Data_Access_Layer.Observer;
using Data_Access_Layer.Observer.Listeners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Data_Access_Layer.Data_Access_Object
{
	public class WagonDAO : IDAO<Wagon>
	{
		private readonly string connectionString;
		private EventManager events = new EventManager();

		public WagonDAO(string connectionString)
		{
			this.connectionString = connectionString;
			events.Attach(new WagonDomainListener());
		}

		public IEnumerable<Wagon> GetAll()
		{
			List<Wagon> wagons = new List<Wagon>();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetWagon", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var wagon = new Wagon()
					{
						id_wagon = Convert.ToInt32(rdr["id_wagon"]),
						fk_train = Convert.ToInt32(rdr["fk_train"]),
						type_wagon = rdr["type_wagon"].ToString(),
						wagon_number = Convert.ToInt32(rdr["wagon_number"]),
						ticket_price = (float)Convert.ToDouble(rdr["ticket_price"])
					};
					wagons.Add(wagon);
				}
				events.Notify("Select entities from");
				return wagons;
			}
		}

		public Wagon GetById(int id)
		{
			Wagon wagon = new Wagon();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetWagon", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					wagon.id_wagon = Convert.ToInt32(rdr["id_wagon"]);
					wagon.fk_train = Convert.ToInt32(rdr["fk_train"]);
					wagon.type_wagon = rdr["type_wagon"].ToString();
					wagon.wagon_number = Convert.ToInt32(rdr["wagon_number"]);
					wagon.ticket_price = (float)Convert.ToDouble(rdr["ticket_price"]);
				}
				events.Notify("Select entity from");
				return wagon;
			}
		}

		public void Insert(Wagon obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spAddNew", con);
				con.Open();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@fk_train", obj.fk_train);
				cmd.Parameters.AddWithValue("@type_wagon", obj.type_wagon);
				cmd.Parameters.AddWithValue("@wagon_number", obj.wagon_number);
				cmd.Parameters.AddWithValue("@ticket_price", obj.ticket_price);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Update(Wagon obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spUpdateWagon", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_wagon", obj.id_wagon);
				cmd.Parameters.AddWithValue("@fk_train", obj.fk_train);
				cmd.Parameters.AddWithValue("@type_wagon", obj.type_wagon);
				cmd.Parameters.AddWithValue("@wagon_number", obj.wagon_number);
				cmd.Parameters.AddWithValue("@ticket_price", obj.ticket_price);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Update entity from");
		}

		public void Delete(Wagon obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spDeleteWagon", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_wagon", obj.id_wagon);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Delete entity from");
		}
	}
}
