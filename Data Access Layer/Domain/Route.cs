﻿using Data_Access_Layer.Memento;
using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Domain
{
	public class Route
	{
		public int id_route { get; set; }
		public string point_of_departure { get; set; }
		public string point_of_arrival { get; set; }
		public DateTime on_way { get; set; }
		public float distance_km { get; set; }

		public List<Train> trains { get; set; }


		public RouteMemento SaveState()
		{
			Console.WriteLine("Saving your route...");
			return new RouteMemento()
			{
				id_route = id_route,
				point_of_departure = point_of_departure,
				point_of_arrival = point_of_arrival,
				on_way = on_way,
				distance_km = distance_km,
				trains = new List<Train>(trains)
			};
		}

		public void RestoreState(RouteMemento routeMemento)
		{
			Console.WriteLine("Restoring your route...");

			id_route = routeMemento.id_route;
			point_of_departure = routeMemento.point_of_departure;
			point_of_arrival = routeMemento.point_of_arrival;
			on_way = routeMemento.on_way;
			distance_km = routeMemento.distance_km;
			trains = new List<Train>(routeMemento.trains);
		}
	}
}