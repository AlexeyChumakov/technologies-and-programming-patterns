﻿using Data_Access_Layer.Data_Access_Object;
using Data_Access_Layer.Singleton;

namespace Data_Access_Layer.Factory
{
	public class WagonDAOFactory : DAOFactory<WagonDAO>
	{
		public override WagonDAO FactoryMethod()
		{
			return WagonDAOSingleton.GetInstance(connectionString);
		}
	}
}
