﻿using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Domain
{
	public class Train
	{
		public int id_train { get; set; }
		public char train_key { get; set; }
		public int fk_route{ get; set; }
		public DateTime departing { get; set; }
		public DateTime arriving { get; set; }

		public List<Wagon> wagons { get; }
	}
}