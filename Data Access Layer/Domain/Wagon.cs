﻿using System.Collections.Generic;

namespace Data_Access_Layer.Domain
{
	public class Wagon
	{
		public int id_wagon { get; set; }
		public int fk_train { get; set; }
		public string type_wagon { get; set; }
		public int wagon_number { get; set; }
		public float ticket_price { get; set; }

		public List<Place> places { get; set; }
	}
}
