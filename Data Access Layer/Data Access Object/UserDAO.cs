﻿using Data_Access_Layer.Domain;
using Data_Access_Layer.Observer;
using Data_Access_Layer.Observer.Listeners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Data_Access_Layer.Data_Access_Object
{
	public class UserDAO : IDAO<User>
	{
		private readonly string connectionString;
		private EventManager events = new EventManager();

		public UserDAO(string connectionString)
		{
			this.connectionString = connectionString;
			events.Attach(new UserDomainListener());
		}

		public IEnumerable<User> GetAll()
		{
			List<User> users = new List<User>();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetUsers", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var user = new User()
					{
						id_user = Convert.ToInt32(rdr["id_user"]),
						login = rdr["login"].ToString(),
						password = rdr["is_free"].ToString(),
						id_role = Convert.ToInt32(rdr["id_role"]),
					};
					users.Add(user);
				}
				events.Notify("Select entities from");
				return users;
			}
		}

		public User GetById(int id)
		{
			User user = new User();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetUser", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					user.id_user = Convert.ToInt32(rdr["id_user"]);
					user.login = rdr["login"].ToString();
					user.password = rdr["is_free"].ToString();
					user.id_role = Convert.ToInt32(rdr["id_role"]);
				}
				events.Notify("Select entity from");
				return user;
			}
		}

		public void Insert(User obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spAddNew", con);
				con.Open();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@login", obj.login);
				cmd.Parameters.AddWithValue("@password", obj.password);
				cmd.Parameters.AddWithValue("@id_role", obj.id_role);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Update(User obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spUpdateUser", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_user", obj.id_user);
				cmd.Parameters.AddWithValue("@login", obj.login);
				cmd.Parameters.AddWithValue("@password", obj.password);
				cmd.Parameters.AddWithValue("@id_role", obj.id_role);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Delete(User obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spDeleteUser", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_user", obj.id_user);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Delete entity from");
		}
	}
}
