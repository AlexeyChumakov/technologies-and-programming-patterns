﻿using System.Collections.Generic;

namespace Data_Access_Layer.Memento
{
    public class RouteHistory
    {
        public Stack<RouteMemento> History { get; private set; }
        public RouteHistory()
        {
            History = new Stack<RouteMemento>();
        }
    }
}
