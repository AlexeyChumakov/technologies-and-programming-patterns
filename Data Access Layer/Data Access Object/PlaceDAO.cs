﻿using Data_Access_Layer.Domain;
using Data_Access_Layer.Observer;
using Data_Access_Layer.Observer.Listeners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Data_Access_Layer.Data_Access_Object
{
	public class PlaceDAO : IDAO<Place>
	{
		private readonly string connectionString;
		private EventManager events = new EventManager();

		public PlaceDAO(string connectionString)
		{
			this.connectionString = connectionString;
			events.Attach(new PlaceDomainListener());
		}

		public IEnumerable<Place> GetAll()
		{
			List<Place> places = new List<Place>();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetPlace", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var place = new Place()
					{
						id_place = Convert.ToInt32(rdr["id_place"]),
						place_number = Convert.ToInt32(rdr["place_number"]),
						is_free = Convert.ToBoolean(rdr["is_free"]),
						fk_wagon = Convert.ToInt32(rdr["fk_wagon"]),
					};
					places.Add(place);
				}
				events.Notify("Select entities from");
				return places;
			}
		}

		public Place GetById(int id)
		{
			Place place = new Place();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("spGetPlace", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					place.id_place = Convert.ToInt32(rdr["id_place"]);
					place.place_number = Convert.ToInt32(rdr["place_number"]);
					place.is_free = Convert.ToBoolean(rdr["is_free"]);
					place.fk_wagon = Convert.ToInt32(rdr["fk_wagon"]);
				}
				events.Notify("Select entity from");
				return place;
			}
		}

		public void Insert(Place obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spAddNew", con);
				con.Open();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@place_number", obj.place_number);
				cmd.Parameters.AddWithValue("@is_free", obj.is_free);
				cmd.Parameters.AddWithValue("@fk_wagon", obj.fk_wagon);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Update(Place obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spUpdatePlace", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_place", obj.id_place);
				cmd.Parameters.AddWithValue("@place_number", obj.place_number);
				cmd.Parameters.AddWithValue("@is_free", obj.is_free);
				cmd.Parameters.AddWithValue("@fk_wagon", obj.fk_wagon);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Insert entity to");
		}

		public void Delete(Place obj)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				var cmd = new SqlCommand("spDeletePlace", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				cmd.Parameters.AddWithValue("@id_place", obj.id_place);
				cmd.ExecuteNonQuery();
			}
			events.Notify("Delete entity from");
		}
	}
}
